
Pod::Spec.new do |s|
  s.name             = 'PositivePay'
  s.version          = '2.0.1'
  s.summary          = 'scan cheque'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/positivepay-ios.git'
  s.license          = 'MIT'
  s.author           = { 'ashish' => 'ashish@frslabs.com' }
  s.source           = { :http => 'https://positivepay-ios.repo.frslabs.space/positivepay-ios/2.0.1/PositivePay.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '12.0'
  s.ios.vendored_frameworks = 'PositivePay.framework'
  s.swift_version = '5.0'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
end